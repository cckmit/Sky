import Vue from 'vue'
import App from './App'
var VueResource = require('vue-resource');
var VueAsyncData = require('vue-async-data')

var VueRouter = require('vue-router');
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueAsyncData)
var rootEl = document.createElement('div');
document.body.appendChild(rootEl);
window.serverUrl = "http://127.0.0.1:8888/";

import DashBoard from './components/system/DashBoard.vue';
import SimpleGrid from './components/framework/SimpleGrid.vue';

import NavBarHeaderCompoment from './components/common/header/NavBarHeaderCompoment.vue';
import NavBarAddonCompoment from './components/common/header/NavBarAddonCompoment.vue'
import MenuConpoment from './components/common/menu/MenuConpoment.vue';
import HeaderCompoment from './components/common/header/HeaderCompoment.vue';
Vue.component('navbar-header', NavBarHeaderCompoment);
Vue.component('navbar-addon', NavBarAddonCompoment);
Vue.component('menu', MenuConpoment);

let Menus = {
  '/dashboard': {
    component: DashBoard
  },
  '/framework/simplegrid/:key/:name': {
    component: SimpleGrid
  },
}

var router = new VueRouter()
router.map(Menus);

$(document.body).addClass("flat-green");
router.start(App, rootEl);
