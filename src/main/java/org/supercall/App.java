package org.supercall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.supercall.common.CommonUtility;
import org.supercall.mybatis.plugins.PaginationInterceptor;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SpringBootApplication
@RequestMapping
@EnableSwagger2
public class App extends WebMvcAutoConfiguration {

    public static void main(String[] args) {
        ApplicationContext appContext = SpringApplication.run(App.class, args);
        CommonUtility.applicationContext = appContext;
        System.out.println("Start Finish");
    }

    @RequestMapping("/dashboards")
    public void dashboards(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.sendRedirect("/index.html");
    }

}
