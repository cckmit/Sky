package org.supercall.controller.sys;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.supercall.common.CommonUtility;
import org.supercall.common.IListPlugin;
import org.supercall.dao.SysConfigMapper;
import org.supercall.domainModel.ResultMessage;
import org.supercall.domainModel.framework.SimpleGridConfigModel;
import org.supercall.models.SysConfig;
import org.supercall.models.SysConfigExample;
import springfox.documentation.spring.web.json.Json;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 简单列表
 */
@RequestMapping("/admin/simplegrid")
@RestController
public class SimpleGridController {
    @Resource
    SysConfigMapper sysConfigMapper;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    HashMap<String, Object> index(
            HttpServletRequest request,
            String uid) throws Exception {
        SysConfig configEntity = CommonUtility.applicationContext.getBean(CommonController.class).loadConfig(uid, null);
        SimpleGridConfigModel simpleGridConfigModel = new Gson().fromJson(configEntity.getMetaconfig(), SimpleGridConfigModel.class);
        IListPlugin extendMapper = (IListPlugin) CommonUtility.applicationContext.getBean(Class.forName(simpleGridConfigModel.getEntityName() + "Extend"));
        HashMap<String, Object> map = CommonUtility.buildSimpleList(extendMapper, request, simpleGridConfigModel);
        return map;
    }

    @RequestMapping(value = "/loadMetaData", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    SimpleGridConfigModel loadMetaData(
            HttpServletRequest request,
            String uid) throws Exception {
        SysConfig configEntity = CommonUtility.applicationContext.getBean(CommonController.class).loadConfig(uid, null);
        SimpleGridConfigModel simpleGridConfigModel = new Gson().fromJson(configEntity.getMetaconfig(), SimpleGridConfigModel.class);
        return simpleGridConfigModel;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    ResultMessage delete(
            HttpServletRequest request,
            String uid,
            Integer id) {
        try {
            SysConfig configEntity = CommonUtility.applicationContext.getBean(CommonController.class).loadConfig(uid, null);
            SimpleGridConfigModel simpleGridConfigModel = new Gson().fromJson(configEntity.getMetaconfig(), SimpleGridConfigModel.class);
            Object mapper = CommonUtility.applicationContext.getBean(Class.forName(simpleGridConfigModel.getEntityName()));
            mapper.getClass().getDeclaredMethod("deleteByPrimaryKey", Integer.class).invoke(mapper, id);
            return new ResultMessage(true, "", "");
        } catch (Exception ex) {
            return new ResultMessage(false, "", "");
        }
    }

    @RequestMapping(value = "/submitEntity")
    @ResponseBody
    @CrossOrigin
    ResultMessage submitEntity(
            HttpServletRequest request,
            String uid,
            String id,
            String content) {
        try {
            Map<String, Object> metaMap = JSON.parseObject(content);
            Object object = CommonUtility.applicationContext.getBean(Class.forName(String.valueOf(metaMap.get("entityName"))));
            Object entity = Class.forName(String.valueOf(metaMap.get("entityName")).replace("Mapper", "").replace("dao", "models")).newInstance();
            List<Map<String, Object>> formConfigs = (List<Map<String, Object>>) metaMap.get("formConfigs");

//            for (Map<String, Object> formConfig : formConfigs) {
//                if (Boolean.valueOf(String.valueOf(formConfig.get("isRequire"))) == false &&
//                        String.valueOf(formConfig.get("value")).equals("")) {
//                    formConfigs.remove(formConfig);
//                }
//            }
            for (Map<String, Object> formConfig : formConfigs) {
                String keyName = String.valueOf(formConfig.get("key"));
                if (formConfig.get("columnType") != null && String.valueOf(formConfig.get("columnType")).equals("Integer")) {
                    if(String.valueOf(formConfig.get("value")).equals("")){
//                        entity.getClass().getMethod("set" + StringUtils.capitalize(keyName), Integer.class).invoke(entity, Integer.valueOf(String.valueOf(formConfig.get("value"))));
                    }else {
                        entity.getClass().getMethod("set" + StringUtils.capitalize(keyName), Integer.class).invoke(entity, Integer.valueOf(String.valueOf(formConfig.get("value"))));
                    }

                } else {
                    entity.getClass().getMethod("set" + StringUtils.capitalize(keyName), String.class).invoke(entity, String.valueOf(formConfig.get("value")));
                }

            }

            if (StringUtils.isBlank(id)) {
                object.getClass().getDeclaredMethod("insertSelective", entity.getClass())
                        .invoke(object, entity);
            } else {
                entity.getClass().getMethod("setId", Integer.class).invoke(entity, Integer.valueOf(id));
                object.getClass().getDeclaredMethod("updateByPrimaryKey", entity.getClass())
                        .invoke(object, entity);
            }
            return new ResultMessage(true, "", "");
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResultMessage(false, "", "");
        }
    }
}
